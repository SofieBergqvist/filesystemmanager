﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using FileSystemManager.LoggingServiceManager;

namespace FileSystemManager.FileServiceManager
{
    class FileManipulation
    {
        //Private Fields
        private readonly string filePath;
        private readonly FileInfo fileInfo = null;
        private readonly Stopwatch stopWatch = new Stopwatch();
        private readonly LoggingService logging = null;

        //Constructor
        public FileManipulation()
        {
            filePath = @"..\..\..\assets\resources\Dracula.txt";
            fileInfo = new FileInfo(filePath);
            logging = new LoggingService();
        }

        //Methods:
        //Get the filename of Dracula.txt using FileInfo.Name
        public string GetFileName()
        {
            stopWatch.Start();

            string fileName = fileInfo.Name;

            stopWatch.Stop();
            logging.WriteLog($@"File name is: {fileName} 
                    The Method took {stopWatch.ElapsedMilliseconds} ms to execute");
            stopWatch.Reset();

            return fileName;
        }

        //Get the size of Dracula.txt using FileInfo.Length
        public long GetFileSize()
        {
            stopWatch.Start();

            long fileSize = fileInfo.Length;

            stopWatch.Stop();
            logging.WriteLog($@"File size: {fileSize} bytes
                    The Method took {stopWatch.ElapsedMilliseconds} ms to execute");
            stopWatch.Reset();

            return fileSize;
        }

        //Get the line-length of Dracula.txt using ReadAllLines method
        public int GetFileLength()
        {
            stopWatch.Start();

            int fileLength = File.ReadAllLines(filePath).Length;

            stopWatch.Stop();
            logging.WriteLog($@"Lines in file: {fileLength} 
                    The Method took {stopWatch.ElapsedMilliseconds} ms to execute");
            stopWatch.Reset();

            return fileLength;
        }

        //Accepts a string from user input and counts the occurences og the string in Dracula.txt 
        public int GetWordCount(string input)
        {
            stopWatch.Start();

            //Read all text and convert to lowercase
            string text = File.ReadAllText(filePath).ToLower();

            //Get index of input and count occurences 
            int index = text.IndexOf(input);
            int wordOcurrence = 0;
            while (index != -1)
            {
                index = text.IndexOf(input, index + 1);
                wordOcurrence++;
            }

            stopWatch.Stop();
            logging.WriteLog($@"The word {input} occured: {wordOcurrence} times. 
                    The Method took {stopWatch.ElapsedMilliseconds} ms to execute");
            stopWatch.Reset();

            return wordOcurrence;
        }

        //Get files from directory, iterate and remove directory path
        public string[] GetFileExtension()
        {
            //File directory and file path
            string path = (@"..\..\..\assets\resources\");
            string[] filePath = Directory.GetFiles(path);

            //empty list collects each file 
            List<string> collectedFiles = new List<string>();
            foreach (string file in filePath)
            {
                collectedFiles.Add(file);
            }

            //new empty list to store edited files from previous list
            List<string> filesExtension = new List<string>();
            foreach (string item in collectedFiles)
            {
                //remove directory path and keep only extension
                string[] split = item.Split('.');
                string lastPart = split.Last();
                filesExtension.Add(lastPart);

            }
            //reuse old list, add new file objects and remove duplicates 
            collectedFiles = filesExtension.Distinct().ToList();

            return collectedFiles.ToArray();

        }

    }
}
