﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FileSystemManager.FileServiceManager
{
    class FileService
    {

        //Private field 
        private readonly string directoryPath;


        //Constructor
        public FileService()
        {
            directoryPath = @"..\..\..\assets\resources";
        }

        //Methods
        //Get the files located in Resource directory using Directory.GetFiles Method
        public string[] GetFiles()
        {
            //Get files from directry and store them in an array 
            string[] filePath = Directory.GetFiles(directoryPath);

            //Empty list to collect folders from iteration
            List<string> collectedFiles = new List<string>();

            //Iterate through array and return all files in directory to List
            foreach (string file in filePath)
            {
                collectedFiles.Add(file);
            }

            return collectedFiles.ToArray();
        }


        //Accepts user input and locates file with matching extension as the user input.
        public string[] GetFilesByExtension(string inputExtension)
        {
            //Get files from directry and store them in an array
            string[] filesByExtension = Directory.GetFiles(directoryPath, "*." + inputExtension);

            //Initialize empty. List
            List<string> collectedFiles = new List<string>();
            foreach (string file in filesByExtension)
            {
                //add  files to List
                collectedFiles.Add(file);
            }

            //Handle error on input
            if (GetSpecialCharacters(inputExtension))
            {
                Console.WriteLine($"\t\tNot valid input. \nRedirect to Main menu ");
            }
            else if (!collectedFiles.Any())
            {
                Console.WriteLine($"\t\tThere are no files with '{inputExtension}' extension. \n\t\tRedirect to Main menu ");
            }

            return collectedFiles.ToArray();
        }

        public bool GetSpecialCharacters(string input)
        {
            return input.Any(ch => !Char.IsLetter(ch));
        }
    }
}
