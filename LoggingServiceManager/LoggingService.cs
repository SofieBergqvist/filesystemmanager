﻿using FileSystemManager.UserInteraction;
using System;
using System.IO;
using System.Linq;

namespace FileSystemManager.LoggingServiceManager
{
    class LoggingService
    {

        // Create textfile (if non exists) stored in project directory 
        //with logged messages from application methods
        public void WriteLog(string logMessage)
        {
            FileStream fileStream = null;

            string logFilePath = @"..\..\..\assets\log\";

            //Create log file with date
            logFilePath = logFilePath + $"Log-{DateTime.Today.ToString("MM-dd-yyyy")}.txt";

            FileInfo logFileInfo = new FileInfo(logFilePath);

            //create new directory in non exists
            DirectoryInfo logDirectory = new DirectoryInfo(logFileInfo.DirectoryName);
            if (!logDirectory.Exists) logDirectory.Create();

            //create new file if no file exists
            if (!logFileInfo.Exists)
            {
                fileStream = logFileInfo.Create();
            }
            else
            {
                //Open and append text in text file
                fileStream = new FileStream(logFilePath, FileMode.Append);
            }

            // Create a writer   open the file
            StreamWriter log = new StreamWriter(fileStream);

            //Write to the file
            log.WriteLine();
            log.WriteLine($"{DateTime.Now} {logMessage}");

            //close stream
            log.Close();
        }

        public static void PrintLog()
        {

            string logFilePath = @"..\..\..\assets\log\";

            //Get the last file from Log directory 
            FileInfo newestFile = GetNewestFile(new DirectoryInfo($"{logFilePath}"));
            string line;

            try
            {
                //Pass file path and file name to  StreamReader 
                StreamReader steamReader = new StreamReader($"{newestFile}");
                //Read the first line of text
                line = steamReader.ReadLine();
                //Continue to read until you reach end of file
                while (line != null)
                {
                    //write the lie to console window
                    Console.WriteLine(line);
                    //Read the next line
                    line = steamReader.ReadLine();
                }
                //close the file
                steamReader.Close();
                Console.WriteLine("\n\nPress any key when done to return to Main Menu");
                Console.ReadLine();
                ApplicationMenu.MainMenu();

            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                Console.WriteLine("Executing finally block.");

            }

        }

        //Method to get the last logged file 
        //using Linq to order and select last logged file 
        public static FileInfo GetNewestFile(DirectoryInfo directory)
        {
            return directory.GetFiles()
                .Union(directory.GetDirectories().Select(d => GetNewestFile(d)))
                .OrderByDescending(f => (f == null ? DateTime.MinValue : f.LastWriteTime))
                .FirstOrDefault();
        }

    }
}
