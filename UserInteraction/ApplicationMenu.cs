﻿using FileSystemManager.FileServiceManager;
using FileSystemManager.LoggingServiceManager;
using System;


namespace FileSystemManager.UserInteraction
{
    class ApplicationMenu
    {

        //Main menu activates MainMenuChoice and interacts with user
        public static void MainMenu()
        {
            FileService fileService = new FileService();
            FileManipulation fileManipulation = new FileManipulation();

            bool exitMenuLoop = false;

            //Display meny content until user exit or enter SubMenu
            while (!exitMenuLoop)
            {
                int userChoice;

                //Execute MainMenu choices
                MainMenuChoice();
                string input = Console.ReadLine();
                //passing user input as argument to activeMenu for reference
                bool activeMenu = int.TryParse(input, out userChoice);

                if (activeMenu)
                {

                    //Output menu item by input from 1-5
                    if (userChoice > 0 & userChoice < 6)
                    {

                        //Listens to and response to user input  
                        switch (userChoice)
                        {
                            case 1:
                                //Iterate over every files in resource directory  
                                //print out file name without the directory path name
                                Console.WriteLine("\n\t\tFiles located in the Resource folder:");
                                foreach (string file in fileService.GetFiles())
                                {
                                    Console.WriteLine($"\t\t{file.Substring(26)}");
                                }
                                break;

                            case 2:
                                Console.WriteLine("\n\t\tFile extensions in directory:");
                                //iterate and display file extension in folder 
                                foreach (string file in fileManipulation.GetFileExtension())
                                {
                                    Console.Write($"\t{file}");
                                }

                                //Listen for user input interate over files in directory 
                                //and displayes files with extension matching user input
                                Console.Write("\n\t\tEnter file extension to display files: ");
                                string userInput = Console.ReadLine();
                                foreach (string file in fileService.GetFilesByExtension(userInput))
                                {
                                    Console.WriteLine($"\t\t{file.Substring(26)}");
                                }
                                break;

                            case 3:
                                //Display SubMenu
                                SubMenu();
                                exitMenuLoop = true;
                                break;

                            case 4:
                                //Print Log
                                LoggingService.PrintLog();
                                break;

                            case 5:
                                Exit();
                                exitMenuLoop = true;
                                break;
                            default:
                                //Main Menu as default
                                MainMenu();
                                break;
                        }
                    }
                    //Else - handle input error 
                    else
                    {
                        Console.WriteLine("\n Please pick a menu choice from 1 to 4");
                    }
                }
                
                else
                {   //If input is not an int 
                    Console.WriteLine("\t\tAn error has occurred");    
                }
                //Wait 2 seconds before display menu choice again
                System.Threading.Thread.Sleep(3200);
            }

        }

        //MainMenuChoice - Displays menuitems to chose between
         static void MainMenuChoice()
        {
            ConsoleStyling();

            //Create new object of class FileService 
            FileService fileService = new FileService();

            //Console Messages displayed when MainMenu is active
            Console.Clear();
            Console.WriteLine("\n\n\t\tFILE SYSTEM MANAGER");
            Console.WriteLine("\t\tMAIN MENU");
            Console.WriteLine("\t\t=====================================");
            Console.WriteLine("\t\tChoose an option:");
            Console.WriteLine("\t\t1) List files in directory");
            Console.WriteLine("\t\t2) Search For files by extension ");
            Console.WriteLine("\t\t3) Manipulate a text file");
            Console.WriteLine("\t\t4) Display Log-file");
            Console.WriteLine("\t\t5) Exit");
            Console.Write("\r\n\t\tSelect an option: ");
        }

        //Main menu activates SubMenuChoice and interacts with user
         static void SubMenu()
        {
            FileManipulation fileManipulation = new FileManipulation();

            bool exitMenuLoop = false;

            //Display meny content until user exit or enter SubMenu
            while (!exitMenuLoop)
            {
                int userChoice;

                //Execute MainMenu choices
                SubMenuChoice();
                string input = Console.ReadLine();
                //passing user input as argument to activeMenu for reference
                bool activeMenu = int.TryParse(input, out userChoice);

                if (activeMenu)
                {

                    //Output menu item by input from 1-5
                    if (userChoice > 0 & userChoice < 6)
                    {

                        //Listens to and response to user input  
                        switch (userChoice)
                        {
                            case 1:
                                Console.WriteLine($"\n\t\tFile size: {fileManipulation.GetFileSize()} bytes");

                                break;
                            case 2:
                                Console.WriteLine($"\n\t\tLines in file: {fileManipulation.GetFileLength()}");

                                break;
                            case 3:
                                Console.Write("\n\t\tEnter search word: ");
                                //Counted occurences is case insensitive
                                string userInput = Console.ReadLine().ToLower();
                                Console.WriteLine($"\t\tThe word {userInput} occurs {fileManipulation.GetWordCount(userInput)} times");
                                break;
                            case 4:
                                MainMenu();
                                exitMenuLoop = true;
                                break;
                            case 5:
                                Exit();
                                exitMenuLoop = true;
                                break;
                            default:
                                MainMenu();
                                break;

                        }
                    }
                    //Else - handle input error 
                    else
                    {
                        Console.WriteLine("\n Please pick a menuchoice from 1 to 4");
                    }
                }

                else
                {   //If input is not an int 
                    Console.WriteLine("An error has occurred");
                }
                //Wait 2 seconds before display menu choice again
                System.Threading.Thread.Sleep(3000);
            }

        }

        //SubMenuChoice - Displays menuitems to chose between
        static void SubMenuChoice()
        {

            //Create new object of class FileManipulation 
            FileManipulation fileManipulation = new FileManipulation();
            
            //Console Messages displayed when SubMenu is active
            Console.Clear();
            Console.WriteLine("\n\n\t\tFILE SYSTEM MANAGER");
            Console.WriteLine("\t\tMANIPULATE FILE");
            Console.WriteLine("\t\t=====================================");
            Console.Write($"\t\tFilename: {fileManipulation.GetFileName()}\n");
            Console.WriteLine("\t\tChoose an option:");
            Console.WriteLine("\t\t1) File size");
            Console.WriteLine("\t\t2) Lines in file ");
            Console.WriteLine("\t\t3) Search for a word ");
            Console.WriteLine("\t\t4) Go back to Main Menu ");
            Console.WriteLine("\t\t5) Exit");
            Console.Write("\r\n\t\tSelect an option: ");

        }

        //Method for terminate console application
        //Gets called from both the MainMenu and the SubMenu
        static void Exit()
        {
            Console.WriteLine("\t\t\nAre you sure you want to exit? y/n");

            if (Console.ReadLine() == "y")
            {
                //Terminate console application with a clean exit
                Environment.Exit(0);
            }

            MainMenu();
        }

        public static void ConsoleStyling()
        {
            Console.Title = "File System Manager";
            Console.BackgroundColor = ConsoleColor.DarkMagenta;


        }
    }
}
