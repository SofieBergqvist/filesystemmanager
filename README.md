# File System Manager
 File System Manager is a Console Applicatio written in C# for reading and manipulating local files

## Features
* Main Menu:
    * List files in resource directory
    * Search for files in resource directory by file extension
    * Option to go to SubMenu
    * Option to display Log-file
* SubMenu
    * Manipulate text file locatet in resource folder
        * Display the file size
        * Display how many lines there are in the file 
        * Search for a specifique word in the text file (case insensitive)   
* A Log file is created from actions taken in SubMenu
    * The log-file is stored under resources/log (
    * Automatic creation of directory and log-file if it doesn't exists
    
### Installation Guide
1. Clone or download repository
2. Open .sln -> build and run
3. .exe file is located in the Debug\netcoreapp3.1 directory
